import json
from collections import Counter

def average(nums):
	return sum(nums) / len(nums)

def avg_len(strs):
	total = 0
	for s in strs:
		total += len(s)
	return total / len(strs)

# def word_frequencies(s):
# 	words = s.split()
# 	frequencies = {}
# 	for word in words:
# 		if not word.isalpha():
# 			continue
# 		if not word in frequencies:
# 			frequencies[word] = 0
# 		frequencies[word] += 1
# 	return frequencies

obj = None
with open("out.json") as f:
	obj = json.load(f)
users = obj["info"]["users"]

# write output to a MarkDown file
md = """# Statistics for {} {}
""".format(obj["info"]["guild_name"], obj["info"]["channel_name"])
md += "Found {} messages\n".format(len(obj["messages"]))

messagesByUser = {}
for message in obj["messages"]:
	author = message["author"]

	if author not in messagesByUser:
		# Filter out members that have left
		if not author in users:
			continue
		messagesByUser[author] = []
	messagesByUser[author].append(message["content"])

md += """Name | Message Count | Avg. Message Length | Unique Words | Unique Words Per Msg. | Most common words
--- | --- | --- | --- | --- | ---
"""

for user, messages in messagesByUser.items():
	username = obj["info"]["users"][user]
	words = filter(str.isalpha, ('\n'.join(messages)).split())
	frequencies = Counter(words).most_common()
	unique = len(frequencies)
	avg = avg_len(messages)

	md += "{} | {} | {:10.1f} | {} | {:10.2f} | {}\n".format(username, len(messages), avg, unique, unique / len(messages), frequencies[:10])

with open("out.md", "w") as f:
	f.write(md)