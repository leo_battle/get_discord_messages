import sys
import time
import requests
import json

if len(sys.argv) != 3:
	print("Usage: {} [user token] [channel id]".format(sys.argv[0]))
	sys.exit()
token = sys.argv[1]
channelID = sys.argv[2]

url = "https://discordapp.com/api/channels/{}/messages".format(channelID)
headers = {"Authorization" : token}

allMessages = []

lastID = 0
while True:
	params = {"limit": 100}
	if len(allMessages) != 0:
		params["before"] = lastID
	response = requests.get(url, headers=headers, params=params)
	messages = json.loads(response.text)
	if len(messages) == 0:
		break
	for message in messages:
		msg = {
			"author" : message["author"]["id"],
			"content" : message["content"],
			"time" : message["timestamp"]
		}
		lastID = message["id"]
		allMessages.append(msg)

print("Found {} messages".format(len(allMessages)))

channel = json.loads(requests.get("https://discordapp.com/api/channels/{}".format(channelID), headers=headers).text)
guild = json.loads(requests.get("https://discordapp.com/api/guilds/{}".format(channel["guild_id"]), headers=headers).text)
members = json.loads(requests.get("https://discordapp.com/api/guilds/{}/members".format(channel["guild_id"]), headers=headers, params={"limit" : 1000}).text)

users = {}
for member in members:
	users[member["user"]["id"]] = member["user"]["username"]

obj = {
	"info" : {
		"guild_name" : guild["name"],
		"guild_id" : guild["id"],
		"channel_name" : channel["name"],
		"channel_id" : channel["id"],
		"users" : users
	},
	"messages" : allMessages
}

with open("out.json", "w") as out:
	json.dump(obj, out)