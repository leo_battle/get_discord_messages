# Get Discord Messages

Use this script to download the entire history of a Discord channel.

The JSON output file was ~3MB for ~25000 messages for my guild.

Usage:
```bash
python3 get_messages.py [Discord user token] [channel ID]
```

To find the user token open the developer console and go to the network tab. Reload the page. Go to the bottom of the list and look for an item called "science" and click on it. This will open a new box with some fields in it. The "Authorization field contains your client token".

## **`WARNING`**
UNDER NO CIRCUMSTANCES ***EVER*** LET ***ANYBODY*** ELSE GET YOUR CLIENT TOKEN. IF SOMEBODY ELSE GETS IT THEY WILL HAVE COMPLETE CONTROL OVER YOUR ACCOUNT

To find the channel id open the channel you want in your browser and in the URL "https://discordapp.com/channels/[this number is the channel id]"